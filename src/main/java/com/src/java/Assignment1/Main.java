package com.src.java.Assignment1;

import org.apache.hadoop.fs.FileSystem;

import java.io.IOException;
import java.net.URI;

public class Main {
    public static void main(String[] args) throws IOException, Exception {

        String localPath = "src/main/resources/Files";
        String uri = "hdfs://localhost:9000";
        String uploadPath = "hdfs://localhost:9000/Assignment1/Files";

        HDFSUploadUtility hdfsUtility = new HDFSUploadUtility();
        hdfsUtility.uploadFilesToHDFS(localPath, uploadPath, uri); //Transfer files from local to HDFS

        HDFSToHBASEUtility utilityUsage = new HDFSToHBASEUtility();

        FileSystem fs = FileSystem.get(new URI(uri), utilityUsage.config);

        utilityUsage.createTable("People_Info");
        utilityUsage.storeInHBASE(fs, uploadPath);


    }
}
