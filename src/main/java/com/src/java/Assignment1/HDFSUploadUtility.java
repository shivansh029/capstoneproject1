package com.src.java.Assignment1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;

public class HDFSUploadUtility {

    public void uploadFilesToHDFS(String localPath,
                                  String uploadPath,
                                  String hdfsURI) throws IOException {

        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(hdfsURI), conf);

        for(int i = 0; i < 100; i++){
            String filePath = localPath + "/File" + i + ".csv";
            fs.copyFromLocalFile(new Path(filePath), new Path(uploadPath));
        }

    }

}
